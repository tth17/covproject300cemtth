# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains the android source files for the project.
* Version 1.3

### How do I get set up? ###

* Clone the repository to local storage using git repository.
* Install Android Studio into your pc and run the program.
* Open the cloned repository folder in the filepath on your local storage.
* Plug your android phone USB cable to connect to the pc and 
* be sure to select the option for USB Debugging in your phone's Developer Menu in settings.
* Click on "Run App" green arrow button on the top right of the android toolbar.
* Android Studios will automatically install the app into your phone and the application will pop up on your screen.

* Make sure that you are using the latest stable version of the android apk, api 25 or lower recommended.

* **Dependencies**
* This application requires Google Maps api.

### Contacts ###

* For any questions regarding the project, please email me.
* email: andrewthanhtut97@gmail.com