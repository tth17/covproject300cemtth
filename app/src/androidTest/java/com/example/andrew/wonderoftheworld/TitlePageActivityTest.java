package com.example.andrew.wonderoftheworld;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.Espresso.onView;

/**
 * Created by ${Thadoe Than Htut} on ${5/12/2017}.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class TitlePageActivityTest {

    @Rule
    public IntentsTestRule<TitlePageActivity> activityTestRule = new IntentsTestRule<>(
            TitlePageActivity.class);

    @Test
    public void menuScreen(){

        onView(withId(R.id.main_button1)).perform(click());
        intended(hasComponent(SevenWonders.class.getName()));
    }

}