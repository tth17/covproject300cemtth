package com.example.andrew.wonderoftheworld;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by ${Thadoe Than Htut} on ${5/12/2017}.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class SevenWondersTest {
    @Rule
    public IntentsTestRule<SevenWonders> SevenTestRule = new IntentsTestRule<>(
            SevenWonders.class);

    @Test
    public void imageScreen() throws Exception {
        onView(withId(R.id.imageList)).perform(click());
        intended(hasComponent(ImageListActivity.class.getName()));
    }

    @Test
    public void showLocation() throws Exception {
        onView(withId(R.id.simpleList)).perform(click());
        intended(hasComponent(MapLocationActivity.class.getName()));

    }

}