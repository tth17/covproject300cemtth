package com.example.andrew.wonderoftheworld;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapLocationActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final String PLACE_NAME = "com.example.andrew.wonderoftheworld.PLACE_NAME";
    public static final String LAT = "com.example.andrew.wonderoftheworld.LAT";
    public static final String LNG = "com.example.andrew.wonderoftheworld.LNG";
    private GoogleMap mMap;
    private String placeName;
    private LatLng latLng;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_location);
        Intent intent = getIntent();
        placeName = intent.getStringExtra(PLACE_NAME);
        String latitude = intent.getStringExtra(LAT);
        String longitude = intent.getStringExtra(LNG);
        latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.locationMap);
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where the marker is added along with the option to zoom into the map. In this case,
     * we have marker parsed from LatLng coordinates declared in class which is parsed from the intent.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //declares the google map ui settings and sets the zoom function
        UiSettings mUiSettings = mMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(true);

        // Gets marker's position and place name from onCreate() method

        mMap.addMarker(new MarkerOptions().position(latLng).title(placeName));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }
}
