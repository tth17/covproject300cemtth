package com.example.andrew.wonderoftheworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.List;

public class SevenWonders extends AppCompatActivity {

    //declaring private variables
    private static final String TAG = "SevenWonders";
    private ListView listView;
    private String[] placeNames;
    private List coorList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seven_wonders);

        // CSVReader is taken from stack overflow
        // https://stackoverflow.com/questions/38415680/how-to-parse-csv-file-into-an-array-in-android-studio/38415815#38415815
        InputStream inputStream = getResources().openRawResource(R.raw.coordinates);
        CSVReader csvFile = new CSVReader(inputStream);
        coorList = csvFile.read();

        //initializes the array variables and sets the array adapter for listView
        placeNames = getResources().getStringArray(R.array.placeNames);
        listView = (ListView) findViewById(R.id.simpleList);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, placeNames);
        listView.setAdapter(arrayAdapter);

        //This method creates the intent showLocation() when simpleList listView is clicked.
        //It will locate the position of the placeNames and the latlng arrays.
        //A Log message will be generated in the Logcat along with a message popup on bottom of screen.
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String[] latlng = (String[]) coorList.get(position);
                        String placeName = placeNames[position];
                        Toast.makeText(getBaseContext(), placeName + " " + latlng[0] + "," + latlng[1], Toast.LENGTH_LONG).show();
                        showLocation(view, latlng, placeName);
                        Log.d(TAG, placeNames + " " + latlng[0] + "," + latlng[1]);
                    }
                }
        );
    }

    //The intent for image list button, bring to a new activity
    public void imageScreen(View view) {
        Intent imageScreen = new Intent(this, ImageListActivity.class);
        startActivity(imageScreen);
    }

    //The intent that parses the name of the place and the LatLng coordinates through the perimeter
    public void showLocation(View view, String[] latlng, String placeName) {
        Intent showLocation = new Intent(this, MapLocationActivity.class);
        showLocation.putExtra(MapLocationActivity.PLACE_NAME, placeName);
        showLocation.putExtra(MapLocationActivity.LAT, latlng[0]);
        showLocation.putExtra(MapLocationActivity.LNG, latlng[1]);
        startActivity(showLocation);
    }

}
