package com.example.andrew.wonderoftheworld;

/**
 * Created by ${Andrew} on ${5/12/2017}.
 */

public class Place {

    //declaring private variables
    private String name;
    private String detail;
    private int image;

    //initialising the parameter values
    public Place(String name, String detail, int image) {
        this.name = name;
        this.detail = detail;
        this.image = image;
    }

    //mtehods return the values in the private variables
    public String getName() {
        return name;
    }

    public String getDetail() {
        return detail;
    }

    public int getImage() {
        return image;
    }

    @Override
    public String toString() {
        return detail;
    }

}
