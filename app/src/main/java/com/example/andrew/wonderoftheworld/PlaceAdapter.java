package com.example.andrew.wonderoftheworld;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ${Andrew} on ${5/12/2017}.
 */

public class PlaceAdapter extends ArrayAdapter<Place> {

    //declaring private variables and array
    private int resource;
    private ArrayList<Place> places;
    private Context context;

    //Constructor for PlaceAdapter class
    public PlaceAdapter(Context context, int resource, ArrayList<Place> places) {
        super(context, resource, places);
        this.resource = resource;
        this.places = places;
        this.context = context;
    }

    //This method implements the Adapter and uses LayoutInflater to fill the view.
    @NonNull
    @Override
    public View getView(int position, View integrateView, ViewGroup parent) {
        View view = integrateView;
        try {
            if (view == null) {
                LayoutInflater layoutInflater = (LayoutInflater)
                        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = layoutInflater.inflate(resource, parent, false);
            }
            //setting the adapter view with the xml layout objects
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            TextView textViewName = (TextView) view.findViewById(R.id.textViewName);
            TextView textViewDetail = (TextView) view.findViewById(R.id.textViewDetail);
            imageView.setImageResource(places.get(position).getImage());
            textViewName.setText(places.get(position).getName());
            textViewDetail.setText(places.get(position).getDetail());
        } catch (Exception e) {
            e.printStackTrace();
            e.getCause();
        }
        return view;
    }
}
