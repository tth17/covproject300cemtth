package com.example.andrew.wonderoftheworld;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class TitlePageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.title_page);
    }

    //creates an intent that will start a new activity and bring up the menu screen in application.
    public void menuScreen(View view) {
        Intent menuScreen = new Intent(this, SevenWonders.class);
        startActivity(menuScreen);
    }

}
