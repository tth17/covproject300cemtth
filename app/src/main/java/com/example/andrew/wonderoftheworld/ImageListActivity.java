package com.example.andrew.wonderoftheworld;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ImageListActivity extends AppCompatActivity {

    private static final String TAG = "ImageListActivity";
    //declaring the images into array placePhotos
    public static int[] placePhotos = {
            R.drawable.great_wall_of_china,
            R.drawable.petra_jordan,
            R.drawable.colosseum,
            R.drawable.chichen_itza,
            R.drawable.machu_piichu,
            R.drawable.taj_mahal,
            R.drawable.christ_the_redeemer
    };
    //declaring private variables and arrays
    private ListView listView;
    private String[] placeNames;
    private String[] placeDetails;
    private List coorList;
    private ArrayList<Place> places = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_list);
        InputStream inputStream = getResources().openRawResource(R.raw.coordinates);
        // CSVReader is taken from stack overflow
        // https://stackoverflow.com/questions/38415680/how-to-parse-csv-file-into-an-array-in-android-studio/38415815#38415815
        CSVReader csvFile = new CSVReader(inputStream);
        coorList = csvFile.read();

        //This initialises the xml objects from layout in class and assign them to arrays.
        placeNames = getResources().getStringArray(R.array.placeNames);
        placeDetails = getResources().getStringArray(R.array.placeDetails);
        listView = (ListView) findViewById(R.id.listViewComplex);
        listView.setAdapter(new PlaceAdapter(this, R.layout.image_adapter_list, places));
        generatePlaces();

        //This method creates the intent showLocation() when listView is clicked.
        //It will locate the position of the placeNames and the latlng arrays.
        //A Log message will be generated in the Logcat along with a message popup on bottom of screen.
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String[] latlng = (String[]) coorList.get(position);
                        String placeName = placeNames[position];
                        Toast.makeText(getBaseContext(), placeName + " " + latlng[0] + "," + latlng[1], Toast.LENGTH_LONG).show();
                        showLocation(view, latlng, placeName);
                        Log.d(TAG, placeNames + " " + latlng[0] + "," + latlng[1]);
                    }
                }
        );
    }

    private void generatePlaces() {
        for (int i = 0; i < placePhotos.length; i++) {
            places.add(new Place(placeNames[i], placeDetails[i], placePhotos[i]));
        }
    }

    public void textScreen(View view) {
        Intent textScreen = new Intent(this, SevenWonders.class);
        startActivity(textScreen);
    }

    public void showLocation(View view, String[] latlng, String placeName) {
        Intent showLocation = new Intent(this, MapLocationActivity.class);
        showLocation.putExtra(MapLocationActivity.PLACE_NAME, placeName);
        showLocation.putExtra(MapLocationActivity.LAT, latlng[0]);
        showLocation.putExtra(MapLocationActivity.LNG, latlng[1]);
        startActivity(showLocation);
    }
}
